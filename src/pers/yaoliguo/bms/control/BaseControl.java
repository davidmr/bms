package pers.yaoliguo.bms.control;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import pers.yaoliguo.bms.entity.SysUser;
import pers.yaoliguo.bms.service.ISysLogService;
import pers.yaoliguo.bms.uitl.Global;


/**
 * @ClassName:       BaseController
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月24日        下午3:23:00
 */
@Controller
public class BaseControl {

	@Autowired
	protected ISysLogService logService;
	
	/**
	 * 获取登录用户
	 * 
	 * @return
	 */
	protected SysUser getLoginUser() {

		Object attribute = RequestContextHolder.currentRequestAttributes()
				.getAttribute(Global.LOGIN_USER_KEY,
						RequestAttributes.SCOPE_SESSION);
		return attribute == null ? null : (SysUser) attribute;
	}
	
	/**
	 * 向session中放数据
	 */
	protected void  setDataToSession(String key, Object value){
		
		RequestContextHolder.currentRequestAttributes()
		.setAttribute(key, value, RequestAttributes.SCOPE_SESSION);
		
	}
	
	/**
	 * 去session中的数据
	 */
	protected Object getDataFromSession(String key){
		
		Object obj = RequestContextHolder.currentRequestAttributes()
		.getAttribute(key, RequestAttributes.SCOPE_SESSION);
		
		return obj;
	}
	
	/**
	 * 删除session中的数据
	 * @param key 标识
	 */
	protected void removeDataFromSession(String key) {
		RequestContextHolder.currentRequestAttributes().setAttribute(key, null,
				RequestAttributes.SCOPE_SESSION);
	}
	
	protected Map getRequestParameter(){
		Map map = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes())
				.getRequest().getParameterMap();  
		return map;
	}
	/*
	 * 自动跳转到主页
	 */
	@RequestMapping(value="/")
	public String skipMenuPage(){
		
		return "redirect:/views/index.html";
	}
}
