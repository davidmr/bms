package pers.yaoliguo.bms.uitl;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DateYMDHMSJsonSerializer extends JsonSerializer<Date>{    
    @Override    
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {    
        try {    
            jsonGenerator.writeString(DateUtil.formatDate(date, DateUtil.DATE_FORMAT_TIME_T));    
        } catch (Exception e) {    
            jsonGenerator.writeString(String.valueOf(date.getTime()));    
        }    
    }    
}    