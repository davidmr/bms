package pers.yaoliguo.bms.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class SysOrganiz implements Serializable{
    private String id;

	private String organizName;

	private String pid;

	private String descript;

	private Boolean del;

	private ArrayList<SysOrganiz> children = new ArrayList<SysOrganiz>();
	
	
	public ArrayList<SysOrganiz> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<SysOrganiz> children) {
		this.children = children;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getOrganizName() {
		return organizName;
	}

	public void setOrganizName(String organizName) {
		this.organizName = organizName == null ? null : organizName.trim();
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid == null ? null : pid.trim();
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript == null ? null : descript.trim();
	}

	public Boolean getDel() {
		return del;
	}

	public void setDel(Boolean del) {
		this.del = del;
	}

	 
}