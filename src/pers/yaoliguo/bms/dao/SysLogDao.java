package pers.yaoliguo.bms.dao;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import pers.yaoliguo.bms.entity.SysLog;

/**
 * @ClassName:       SysLogDao
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月5日        下午8:14:21
 */
@Repository("sysLogDao")
public interface SysLogDao {
	
	int recordLog(SysLog log);
	
	SysLog getById(String id);
	
	List<Map> getAllLog();

}
