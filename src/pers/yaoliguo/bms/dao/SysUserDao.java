package pers.yaoliguo.bms.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.SysUser;
@Repository("sysUserDao")
public interface SysUserDao {
    int deleteByPrimaryKey(String id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);
    
    List<SysUser> selectAll(Map map);
    
    int selectCount(Map map);
}