package pers.yaoliguo.bms.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.SysRole;
@Repository("sysRoleDao")
public interface SysRoleDao {
	
    int deleteByPrimaryKey(String id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);
    
    int selectCount(Map map);
    
    List<SysRole> selectAll(Map map);
    
    int updateByPID(SysRole record);
    
}