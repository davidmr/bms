require(['vue','ELEMENT','base','vue-resource','Message'],function(Vue,ELEMENT,baseUtil,VueResource,messageHelper){
	Vue.use(VueResource);
	Vue.use(ELEMENT);
	Vue.use(messageHelper);
	var deploy = new Vue({
		el:"#deployList",
		data:{
			tableData:[],
			getURL:baseUtil.WebRootUrl+"/ModelControl/deploy/getDeployList",
			viewImg:baseUtil.WebRootUrl+"/ModelControl/viewProcessImg",
			deleteProUrl:baseUtil.WebRootUrl+"/ModelControl/deleteDepolyProcess"
		},
		mounted:function(){
			this.getData();
		},
		methods:{
			getData(){
				vm = this;
				vm.$http.get(vm.getURL)
				.then((response) => {
					if(response.data.result == "200"){
		    			 vm.tableData = response.data.dataList;
		    		 }else{
		    			 console.log(response);
		    		 }
				})
				.catch((response) => {
					 console.log(response);
				})
			},
			dateFormat(row,column){
				var date = row[column.property];
				if(date == null){
					return "";
				}else{
					return new Date(date).format("yyyy-MM-DD HH:mm:ss");
				}
		   },
		   viewProcessImg(index, row){
			   window.open(vm.viewImg+"?depId="+row.id);
		   },
		   deleteProcess(idex,row){
			   vm = this;
				vm.$http.post(vm.deleteProUrl,{"depId":row.id},{emulateJSON:true})
				.then((response) => {
					if(response.data.result == "200"){
		    			 vm.tableData = response.data.dataList;
		    			 vm.getData();
		    			 vm.$showMess({message:'删除成功!',messType:'success'});
		    		 }else{
		    			 console.log(response);
		    			 vm.$showMess({message:'删除失败!',messType:'error'});
		    		 }
				})
				.catch((response) => {
					 console.log(response);
					 vm.$showMess({message:'删除失败!',messType:'error'});
				})
		   }
		}
	})
});