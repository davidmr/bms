require(['vue','ELEMENT','base','vue-resource','Message'],
		function(Vue,ELEMENT,baseUtil,VueResource,messageHelper){
	 //安装 vue的ajax功能
    Vue.use(VueResource);
    //安装element
    Vue.use(ELEMENT);
    //安装message
    Vue.use(messageHelper);
  //检测表单
	var checkMenuName = function(rule, value, callback){
             if(value == ''){
            	 callback(new Error('菜单名不能为空！'));
             }
             else{
            	 callback();
             }
	};
	 
    var menuList = new  Vue({
    	el:"#menuList",
    	 watch: {
    	      filterText:function(val) {
    	        this.$refs.bmsmenumanage.filter(val);
    	      }
    	    },
    	data:{
    		  
            
    		data:[
    		      {pid:'',id:"-1",menuName:"一级菜单",icon:"",children:[]},
    		     
    		     ],
    		    filterText: '',
    		     defaultProps: {  
 	                children: 'children',  
 	                label: 'menuName', 
 	                value:'id'
 	            }  ,
    		    
    		menu:{
    			id:"",
    			pid:"",
    			menuName:"",
    			icon:"",
    			url:"",
    			seq:"",
    			state:true,
    			pname:"" 
    		},
    	    getURL:baseUtil.WebRootUrl+"/SysMenuControl/getMenuList",
    	    addMenuURL:baseUtil.WebRootUrl+"/SysMenuControl/addMenu",
    	    updateMenuURL:baseUtil.WebRootUrl+"/SysMenuControl/updateMenu",
    	    deleteMenuURL:baseUtil.WebRootUrl+"/SysMenuControl/deleteMenu",
    	    info:"",
    	    bmsMenuVisible: false,
    	    formLabelWidth: '120px',
    	    bmsMenuTitle:"",
    	    optionDefalut:[''],//下拉列表的默认值。
    	    status:0, // 0: 添加  1:修改,
    	    delArr:[],
    	    expandAllNodes:true,
    	    selectIcon:false,
    	    iconFonts:[],
    	    menuRule:{
    	    	menuName:[{
    	    		validator:checkMenuName,trigger: 'blur'
    	    	}] 
    	    }
    	    
    	},
    	mounted:function(){
    		this.getIcons();
    		this.getData();
    	},
    	methods:{
    		
    		//操作节点的方法开始 ++++++++++++++++++
    		expandAll:function(){
    			this.getData();
    		},
    		collapseAll:function(){
    			this.expandAllNodes = false;
    			this.getData();
    		},
    		nodeClick:function(){
    			console.log(arguments);
    		},
            //操作节点的方法结束-------------------
            //操作下拉列表的方法开始+++++++++++++++++++++
            //当点击select时候触发的方法
            getSelectNode:function(optionValues){
            	 this.menu.pid = optionValues[optionValues.length-1];
            },
            //加载节点的父节点的默认路径
            getParentDataPath:function(node){
            	this.optionDefalut.length = 0;//为了加载select默认值，首先把存放默认路径的数组清空。
            	this.getDataPath(node); //加载节点的路径 ps:elementUI的默认value设置太恶心了，必须得是节点的路径才行。。所以就有了这么个方法=。=
            	this.optionDefalut.shift();
            	this.optionDefalut.reverse();
            },
            //递归求出菜单的路径
            getDataPath:function(node){
          	  
          	  return node.key=='-1'?this.optionDefalut.push(node.key):this.getDataPath(node.parent,this.optionDefalut.push(node.key));  
              
            },
            //编辑状态下禁用当前节点
            changeStateSelect:function(arr,id){
            	var f = false;
            	for(var i = 0 ;i<arr.length&&!f;i++){
                      if(arr[i].id == id){
                    	  arr[i].disabled = true;
                    	  f = true;
                    	  return f;
                      }
                      else if(arr[i].children)
                    	  {
                    f = this.changeStateSelect(arr[i].children,id);
                    	  }
            	}
            	return f;
            },
            //启用所有select
            enableAllSelects:function(arr){
            	for(var i = 0 ;i<arr.length;i++){
            		 arr[i].disabled = false;
            		if(arr[i].children){
            			this.enableAllSelects(arr[i].children);	
            		}
            	}
            },
            //操作下拉列表的方法结束----------------------
            //操作table的方法开始++++++++++++++++++++
            iconChoose:function(obj){
            	//console.log(arguments);
            	this.menu.icon = obj.class;
            	this.selectIcon = false;
            },
            //操作table的方法结束---------------------
            //节点的渲染函数、此处可对节点进行一些操作,相当于vue里的render函数
            renderContent:function(createElement, { node, data, store }) {  
                var self = this;  
                return createElement('span',{attrs:{
                	style:'display: inline-flex;width:calc(100% - 33px);justify-content: space-between;'
                }}, [  
                    createElement('span', node.label),  
                    createElement('span', {attrs:{  
                        style:"float: right; margin-right: 20px"  
                    }},[  
                        !data.url?createElement('el-button',{attrs:{  
                            size:"mini",
                            title:'添加此节点的子节点'
                        },on:{  
                            click:function() {  
                               // console.info("点击了节点" + data.id + "的添加按钮");
                              //  console.log(node);
                                self.bmsMenuTitle = "添加子菜单" //设置菜单标题
                                self.enableAllSelects(self.data);
                                for(key in self.menu){
                                	self.menu[key] = "";
                                }
                                self.menu.pid = node.key;//父级菜单的id	
                                self.status = 0;//设置为添加状态
                                self.optionDefalut.length = 0;
                                self.getDataPath(node); 
                                self.optionDefalut.reverse();
                                self.menu.state = true;
                                self.bmsMenuVisible = true;//开启对话框
                               //store.append({ id: self.baseId++, label: 'testtest', children: [] }, data);  
                            }  
                        }},[createElement('i',{
                        	attrs:{
                        		class:'el-icon-plus'
                        	}
                        })]):"",  
                        data.id!='-1'?createElement('el-button',{attrs:{  
                            size:"mini",
                            title:'删除'
                        },on:{  
                            click:function() {  
                            	
                                //console.info("点击了节点" + data.id + "的删除按钮");  
                                //store.remove(data);  
                            	self.delArr.length = 0;
                            	self.delArr.push({id:node.key});
                            	//调用删除方法删除数组delArr里面的菜单。
                            	self.delMenu();
                            }  
                        }},[createElement('i',{
                        	attrs:{
                        		class:'el-icon-delete'
                        	}
                        })]):"",  
                        //对菜单进行编辑操作。
                        data.id!='-1'? createElement('el-button',{attrs:{  
                            size:"mini",
                            title:'编辑'
                        },on:{  
                            click:function() {  
                                //console.info("点击了节点" + data.id + "的删除按钮");  
                            	self.bmsMenuTitle = "编辑子菜单" //设置菜单标题
                        		self.enableAllSelects(self.data);//启用所有节点
                        		self.getParentDataPath(node);//加载默认父节点 
                            //当前菜单的数据赋给menu
                            	for(key in self.menu){
                            		self.menu[key] = data[key];
                            	} 
                            	self.status = 1;//设置为编辑状态
                            	self.changeStateSelect(self.data,self.menu.id);//禁用当前的项目（自己不能成为自己的父节点）
                            	self.bmsMenuVisible = true;//开启对话框
                            }  
                        }},[createElement('i',{
                        	attrs:{
                        		class:'el-icon-edit'
                        	}
                        })]):""]),  
                ]);  
            }  
        ,
        //对节点进行过滤筛选操作的方法
        filterNode:function(value, data) {
            if (!value) return true;
            return data.menuName.indexOf(value) !== -1;
          },
          //对菜单的数据进行一系列的操作开始+++++++++++++++++++++++++++
          //从后台通过ajax的方法获取节点数据
    		getData:function(){
    			vm = this;
    			vm.$http.get(vm.getURL,{"pid":"-1"})
    			.then((response) => {
    				vm.info = response.data.info;
    				if(response.data.result == "200"){
    				  //添加一级菜单
    				 vm.data[0].children.length = 0;
    				 vm.removeChilren(response.data.dataList);
    				 vm.$set(vm.data[0],'children',response.data.dataList); 	 
    				                         
    				}
    			})
    			.catch(function(response) {
    				vm.info="有异常了";
    			})
    		},
    		//删除选择的节点
    		delChooseNode:function(){
    			this.delArr = this.$refs.bmsmenumanage.getCheckedKeys().map(function(item){
    				return {id:item};
    			});
    			this.delMenu();
    		} ,
    		//删除菜单
    		delMenu:function(){
    			vm = this;
    			var delFlag = 0;//删除标志  0:全部失败   1:全部成功 2:部分成功
    			var message;
    			var type;
    			vm.$showConfirm({
            		title:'删除确认窗口',
            		tips:'是否删除？',
            		ok:'确定',
            		no:'取消',
            		success:function(){
            		if(vm.delArr.length == 0){
            			if(vm.delArr.length == 0){
                			message = '请先选择要删除的菜单！';
                			type = 'info';
                			vm.$showMess({message:message,messType:type});
            			 }
            		}	
            		else{
            			var proArr = vm.delArr.reduce(function(pre,cur){
            				return pre.push(baseUtil.ajaxPromise(vm.deleteMenuURL,cur,'post',true,{Json:true,success:'success',error:'error'})),pre;
            			},[]);
            			Promise.all(proArr).then(function(arr){
                			for(var i = 0; i<arr.length;i++){
                				if(arr[i] == 'success' && delFlag != 2){
                					delFlag = 1;
                				}
                				else if(arr[i] == 'error' && delFlag == 1){
                					delFlag = 2;
                				}
                			}
                			if(delFlag == 0){
                				message = '删除失败！';
                				type = 'error';
                			}
                			else if(delFlag == 1){
            				    message = '删除成功'
            				    type = 'success';
            				}
                			else{
                				message = '删除部分成功！';
                				type = 'info';
                			}
                			vm.$showMess({message:message,messType:type});
           				    vm.getData();
           	    			vm.bmsMenuVisible = false;
                		});	 
            		}
            		
            			
            		},
            		error:function(){
            			vm.$showMess({message:'取消删除!',messType:'error'});
            		}
            	});
    			
    		},
    		//删除所选菜单
    		delAllMenu:function(){
    			this.delArr = this.setCheckedKeys(); 
    			this.delMenu();
    		},
    		//关掉菜单
    		resetMenu:function(){
    			vm = this;
    			vm.bmsMenuVisible = false;
    		},
    		
    		//保存菜单
    		saveMenu:function(){
    			vm = this;
    			var flag = false;
    			//先检查表单是否通过
    			vm.$refs.menuForm.validate(function(valid){
    				 if (valid) {
    					 vm.menu.del = 0;
    		    			vm.$http.post(vm.status==0?vm.addMenuURL:vm.updateMenuURL,vm.menu,{emulateJSON:true})
    		    			.then(function(response){
    		    				vm.info = response.data.info;
    		    				 this.$showMess({messType:'success',message:(vm.status==0?'添加':'修改')+'菜单成功！'});
    		    				if(vm.status){
    		    					vm.changeStateSelect(vm.data,vm.menu.id);//禁用当前的项目（自己不能成为自己的父节点）
    		                     }
    		    				 vm.getData();
    		    	    			vm.bmsMenuVisible = false;
    		     			})
    		    			.catch(function(response){
    		    				vm.info = response;
    		    				if(vm.status){
    		    					vm.changeStateSelect(vm.data,vm.menu.id);//禁用当前的项目（自己不能成为自己的父节点）
    		                     }
    		    				vm.getData();
    		        			vm.bmsMenuVisible = false;
    		    				  this.$showMess({messType:'error',message:'人品不行！'+(vm.status==0?'添加':'修改')+'失败！'});
    		    			});
    			          } 
    			}); 
    			 
    			
    		} ,
    		//移除数据中children的当children的length为0时
    		removeChilren:function(arr){
    			  let i = 0;
    			  for(;i<arr.length;i++){
    				  if(arr[i].children.length > 0){
    					  this.removeChilren(arr[i].children);
    				  }
    				  else{
    					  delete arr[i].children;
    				  }
    			  }
    		},
    		//获取图标
    		getIcons:function(){
    			var iconList = document.getElementById("icon-list");
    			for(var i = 0 ;i < iconList.childNodes.length; i++){
    			var className = iconList.childNodes[i].innerText.trim();
    			this.iconFonts.push({
    				class:className,
    				icon:className
    			});
    			}
    		}
    		//对菜单的数据进行一系列的操作结束--------------
    	}
    });
});

